package com.example.cicd;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class CiCdApplication {

    public static void main(String[] args) {
        SpringApplication.run(CiCdApplication.class, args);

    }
    @Bean
    public CommandLineRunner coucou() {
        return args -> {
            System.out.println("coucou");

        };
    }


}
